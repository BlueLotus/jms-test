package com.sishu.jmstest.broker.util;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.MessageTransformer;

public class SimpleMessageTransformer implements MessageTransformer{

	@Override
	public Message producerTransform(Session session, MessageProducer producer,
			Message message) throws JMSException {
		MapMessage mapMessage = session.createMapMessage();
		mapMessage.setString(((TextMessage)message).getText(), "Value: " + ((TextMessage)message).getText());
		return mapMessage;
	}

	@Override
	public Message consumerTransform(Session session, MessageConsumer consumer,
			Message message) throws JMSException {
		// TODO Auto-generated method stub
		return null;
	}

}
