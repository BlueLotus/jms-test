package com.sishu.jmstest.broker.util;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class ConnectionConstantList {
	
	public final static String WIRED_IP = "192.168.1.102";
	public final static String WIRELESS_IP = "172.20.10.7";
	
	public final static String ADMIN_PORT = ":8161";
	public final static String TCP_PORT1 = ":61616";
	public final static String TCP_PORT2 = ":61716";
	
	public final static String WIRED_CONNECTION_FOR_BROKER = "tcp://" + WIRED_IP;
	public final static String WIRELESS_CONNECTION_FOR_BROKER = "tcp://" + WIRELESS_IP;
	
	public final static String WIRED_CONNECTION_FOR_FAILOVER_CLUSTER 
	= "failover:(" + WIRED_CONNECTION_FOR_BROKER + TCP_PORT1 + "," 
				   + WIRED_CONNECTION_FOR_BROKER + TCP_PORT2 + ")?randomize=false";
	
	public final static String WIRELESS_CONNECTION_FOR_FAILOVER_CLUSTER 
	= "failover:(" + WIRELESS_CONNECTION_FOR_BROKER + TCP_PORT1 + "," 
				   + WIRELESS_CONNECTION_FOR_BROKER + TCP_PORT2 + ")?randomize=false";
	
	public final static String WIRED_BROKER1 = WIRED_CONNECTION_FOR_BROKER + TCP_PORT1;
	public final static String WIRED_BROKER2 = WIRED_CONNECTION_FOR_BROKER + TCP_PORT2;
	
	public final static String WIRELESS_BROKER1 = WIRELESS_CONNECTION_FOR_BROKER + TCP_PORT1;
	public final static String WIRELESS_BROKER2 = WIRELESS_CONNECTION_FOR_BROKER + TCP_PORT2;
	
	public final static String WIRED_BROKER_WITH_FILE_SERVER =
			WIRED_BROKER1 + "?jms.blobTransferPolicy.uploadUrl=http://" + WIRED_IP + ADMIN_PORT + "/fileserver/";
	public final static String WIRELESS_BROKER_WITH_FILE_SERVER =
			WIRELESS_BROKER1 + "?jms.blobTransferPolicy.uploadUrl=http://" + WIRELESS_IP + ADMIN_PORT + "/fileserver/";
	
	
}
