package com.sishu.jmstest.broker;

import java.net.URI;

import org.apache.activemq.broker.BrokerFactory;
import org.apache.activemq.broker.BrokerService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class InnrBroker {
	
	BrokerService broker;
	ApplicationContext ctx;
	
	public static void main(String[] args) throws Exception {
		
		InnrBroker innrBroker = new InnrBroker();
		//innrBroker.getBrokerWithCode();
		//innrBroker.getBrokerWithPropFile();
		innrBroker.getBrokerWithSpring();
		
	}
	
	public void getBrokerWithSpring() {
		ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
	}
	
	public void getBrokerWithCode() throws Exception {
		broker = new BrokerService();
		broker.setUseJmx(true);
		broker.addConnector("tcp://localhost:61616");
		broker.start();
	}
	
	public void getBrokerWithPropFile() throws Exception {
		String url = "properties:broker.properties";
		broker = BrokerFactory.createBroker(new URI(url));
		broker.addConnector("tcp://localhost:61616");
		broker.start();
	}

}
