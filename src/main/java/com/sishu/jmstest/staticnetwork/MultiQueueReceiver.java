package com.sishu.jmstest.staticnetwork;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import com.sishu.jmstest.broker.util.ConnectionConstantList;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class MultiQueueReceiver {
	
	public static void main(String[] args) throws Exception {
		ConnectionFactory connectionFactory = 
				new ActiveMQConnectionFactory(ConnectionConstantList.WIRED_CONNECTION_FOR_FAILOVER_CLUSTER);
		for(int i = 0; i < 5; i++) {
			Thread thread = new ReceiveThread(connectionFactory);
			thread.start();
			Thread.sleep(1000L);
		}
		
	}
	
}

class ReceiveThread extends Thread {
	
	private ConnectionFactory connectionFactory;
	
	public ReceiveThread(ConnectionFactory connectionFactory) {
		this.connectionFactory = connectionFactory;
	}
	
	public void run() {
		try {
				Connection connection = connectionFactory.createConnection();
				connection.start();
				
				final Session session = connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);
				
				Destination destination = session.createQueue("Consumer.A.VirtualTopic.Gossip");
				
				MessageConsumer consumer = session.createConsumer(destination);
				
				consumer.setMessageListener(new MessageListener() {
					
					@Override
					public void onMessage(Message message) {
						TextMessage textMessage = (TextMessage) message;
						
						try {
							System.out.println("Receive msg: " + textMessage.getText());
							session.commit();
							session.close();
							connection.close();
						} catch (JMSException e) {
							e.printStackTrace();
						}

					}
				});
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}