package com.sishu.jmstest.queue;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MapMessage;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import com.sishu.jmstest.broker.util.ConnectionConstantList;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class QueueSender {
	
	public static void main(String[] args) throws Exception {
		
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ConnectionConstantList.WIRED_BROKER1);
		Connection connection = connectionFactory.createConnection();
		connection.start();
		
		Session session = connection.createSession(Boolean.TRUE, Session.CLIENT_ACKNOWLEDGE);
		
		Destination destination = session.createQueue("my-queue");
		
		MessageProducer producer = session.createProducer(destination);
		
		for(int i = 0; i < 3; i++) {
			TextMessage message = session.createTextMessage("Message: " + i);
			/*MapMessage mapMessage = session.createMapMessage();
			mapMessage.setStringProperty("extraMsg" + i, "Network Yooooooooooo!!!");
			mapMessage.setString("Msg" + i, "Map Msg" + i);*/
			producer.send(message);
		}
		
		session.commit();
		session.close();
		connection.close();
	}

}
