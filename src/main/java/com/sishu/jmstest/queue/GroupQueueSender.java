package com.sishu.jmstest.queue;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MapMessage;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import com.sishu.jmstest.broker.util.ConnectionConstantList;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class GroupQueueSender {
	
	public static void main(String[] args) throws Exception {
		
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ConnectionConstantList.WIRELESS_BROKER1);
		Connection connection = connectionFactory.createConnection();
		connection.start();
		
		Session session = connection.createSession(Boolean.TRUE, Session.CLIENT_ACKNOWLEDGE);
		Destination destination = session.createQueue("group-queue");
		MessageProducer producer = session.createProducer(destination);
		
		for(int i = 0; i < 3; i++) {
			TextMessage messageA = session.createTextMessage("Message for group A: " + i);
			messageA.setStringProperty("JMSXGroupID", "GroupA");
			producer.send(messageA);
			
			TextMessage messageB = session.createTextMessage("Message for group B: " + i);
			messageB.setStringProperty("JMSXGroupID", "GroupB");
			producer.send(messageB);
		}
		
		session.commit();
		session.close();
		connection.close();
	}

}
