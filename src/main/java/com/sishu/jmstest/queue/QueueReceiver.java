package com.sishu.jmstest.queue;

import java.util.Enumeration;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MapMessage;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import com.sishu.jmstest.broker.util.ConnectionConstantList;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class QueueReceiver {
	
	public static void main(String[] args) throws Exception {
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ConnectionConstantList.WIRED_BROKER1);
		Connection connection = connectionFactory.createConnection();
		connection.start();
		
		Enumeration<String> names = connection.getMetaData().getJMSXPropertyNames();
		
		while(names.hasMoreElements()) {
			String name = (String) names.nextElement();
			System.out.println("JMSX namd: " + name);
		}
		
		final Session session = connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);
		
		Destination destination = session.createQueue("my-queue");
		
		MessageConsumer consumer = session.createConsumer(destination);
		
		int i = 0;
		while(i < 3) {
			
			//TextMessage message = (TextMessage) consumer.receive();
			MapMessage mapMessage = (MapMessage) consumer.receive();
			
			session.commit();
			
			System.out.println("Receive: " + mapMessage.getString("Msg" + i) + 
					", prop: " + mapMessage.getStringProperty("extraMsg" + i));
			i++;
		}
		
		session.close();
		connection.close();
	}
	
}
