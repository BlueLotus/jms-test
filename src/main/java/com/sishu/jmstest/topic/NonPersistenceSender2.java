package com.sishu.jmstest.topic;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import com.sishu.jmstest.broker.util.ConnectionConstantList;

/**
 * 
 * @author Carl Adler(C.A.)
 * 
 * For message dispatch feature test.
 *
 */
public class NonPersistenceSender2 {
	
	public static void main(String[] args) throws Exception {
		
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ConnectionConstantList.WIRED_BROKER1);
		Connection connection = connectionFactory.createConnection();
		connection.start();
		
		Session session = connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);
		
		Destination destination = session.createTopic("my-topic");
		
		for(int i = 0; i < 2; i++) {
			MessageProducer producer = session.createProducer(destination);
			for(int j = 0; j < 10; j++) {
				TextMessage message = session.createTextMessage(i + " Message-" + j);
				producer.send(message);
			}
		}
		
		session.commit();
		session.close();
		connection.close();
	}

}
