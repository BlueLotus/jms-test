package com.sishu.jmstest.topic;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicSubscriber;

import org.apache.activemq.ActiveMQConnectionFactory;

import com.sishu.jmstest.broker.util.ConnectionConstantList;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class PersistenceReceiver {
	
	public static void main(String[] args) throws Exception {
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ConnectionConstantList.WIRELESS_BROKER1);
		Connection connection = connectionFactory.createConnection();
		connection.setClientID("carl_1");
		
		final Session session = connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);
		
		Topic destination = session.createTopic("Gossip");
		TopicSubscriber topicSubscriber = session.createDurableSubscriber(destination, "topic_1");
		connection.start();
		
		Message message = topicSubscriber.receive();
		
		while (message != null) {
			TextMessage textMessage = (TextMessage) message;
			System.out.println("Receive msg: " + textMessage.getText());
			message = topicSubscriber.receive(1000L);
		}
		
		session.commit();
		session.close();
		connection.close();
	}
	
}
