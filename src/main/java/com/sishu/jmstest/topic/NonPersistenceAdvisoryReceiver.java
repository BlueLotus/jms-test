package com.sishu.jmstest.topic;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQMessage;
import org.apache.activemq.command.ProducerInfo;

import com.sishu.jmstest.broker.util.ConnectionConstantList;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class NonPersistenceAdvisoryReceiver {
	
	public static void main(String[] args) throws Exception {
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ConnectionConstantList.WIRED_BROKER1);
		Connection connection = connectionFactory.createConnection();
		connection.start();
		
		final Session session = connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);
		
		Destination destination = session.createTopic("ActiveMQ.Advisory.Producer.Topic.my-topic");
		
		MessageConsumer consumer = session.createConsumer(destination);
		
		Message message = consumer.receive();
		
		while (message != null) {
			if(message instanceof ActiveMQMessage) {
				try {
					ActiveMQMessage activeMQMessage = (ActiveMQMessage) message;
					ProducerInfo producerInfo = (ProducerInfo) activeMQMessage.getDataStructure();
					System.out.println("producer count: " + activeMQMessage.getProperty("producerCount"));
					System.out.println("producer id: " + producerInfo.getProducerId());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			message = consumer.receive(1000L);
		}
		
		session.commit();
		session.close();
		connection.close();
	}
	
}
