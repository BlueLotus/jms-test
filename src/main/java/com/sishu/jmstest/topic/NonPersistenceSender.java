package com.sishu.jmstest.topic;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ScheduledMessage;

import com.sishu.jmstest.broker.util.ConnectionConstantList;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class NonPersistenceSender {
	
	public static void main(String[] args) throws Exception {
		
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ConnectionConstantList.WIRED_BROKER1);
		Connection connection = connectionFactory.createConnection();
		connection.start();
		
		Session session = connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);
		
		Destination destination = session.createTopic("my-topic");
		
		MessageProducer producer = session.createProducer(destination);
		
		for(int i = 0; i < 3; i++) {
			TextMessage message = session.createTextMessage("Message-" + i);
			long delay = 3 * 1000;
			long period = 3 * 1000;
			int repeat = 5;
			message.setLongProperty(ScheduledMessage.AMQ_SCHEDULED_DELAY, delay);
			message.setLongProperty(ScheduledMessage.AMQ_SCHEDULED_PERIOD, period);
			message.setIntProperty(ScheduledMessage.AMQ_SCHEDULED_REPEAT, repeat);
			producer.send(message);
		}
		
		session.commit();
		session.close();
		connection.close();
	}

}
