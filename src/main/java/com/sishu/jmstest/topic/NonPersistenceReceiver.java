package com.sishu.jmstest.topic;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import com.sishu.jmstest.broker.util.ConnectionConstantList;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class NonPersistenceReceiver {
	
	public static void main(String[] args) throws Exception {
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ConnectionConstantList.WIRED_BROKER1);
		Connection connection = connectionFactory.createConnection();
		connection.start();
		
		final Session session = connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);
		
		Destination destination = session.createTopic("my-topic");
		
		MessageConsumer consumer = session.createConsumer(destination);
		
		Message message = consumer.receive();
		
		while (message != null) {
			TextMessage textMessage = (TextMessage) message;
			System.out.println("Receive msg: " + textMessage.getText());
			message = consumer.receive();
		}
		
		session.commit();
		session.close();
		connection.close();
	}
	
}
