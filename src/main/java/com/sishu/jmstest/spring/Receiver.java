package com.sishu.jmstest.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Service
public class Receiver {
	
	@Autowired
	private JmsTemplate jmsTemplate;

	public static void main(String[] args) {
		
		ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		Receiver receiver = (Receiver) ctx.getBean("receiver");
		
		String msg = (String) receiver.jmsTemplate.receiveAndConvert();
		
		System.out.println("Receive: " + msg);
		
	}

}
