package com.sishu.jmstest.spring;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Service
public class Sender {
	
	@Autowired
	private JmsTemplate jmsTemplate; 

	public static void main(String[] args) throws Exception {
		
		ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		Sender sender = (Sender) ctx.getBean("sender");
		
		sender.jmsTemplate.send(new MessageCreator() {
			@Override
			public Message createMessage(Session session) throws JMSException {
				TextMessage message = session.createTextMessage("Msg from spring");
				return message;
			}
		});

	}

}
