package com.sishu.jmstest.spring;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class MyMessageListener implements MessageListener {

	@Override
	public void onMessage(Message message) {
		TextMessage textMessage = (TextMessage) message;
		try {
			System.out.println("Receive msg: " + textMessage.getText());
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

}
