package com.sishu.jmstest.message;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ActiveMQMessageProducer;
import org.apache.activemq.MessageTransformer;

import com.sishu.jmstest.broker.util.ConnectionConstantList;
import com.sishu.jmstest.broker.util.SimpleMessageTransformer;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class QueueSenderWithMsgTransformer {
	
	public static void main(String[] args) throws Exception {
		
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ConnectionConstantList.WIRELESS_BROKER1);
		Connection connection = connectionFactory.createConnection();
		connection.start();
		
		Session session = connection.createSession(Boolean.TRUE, Session.CLIENT_ACKNOWLEDGE);
		
		Destination destination = session.createQueue("my-queue-2");
		
		ActiveMQMessageProducer producer = (ActiveMQMessageProducer) session.createProducer(destination);
		MessageTransformer transformer = new SimpleMessageTransformer();
		for(int i = 0; i < 3; i++) {
			TextMessage message = session.createTextMessage("Message: " + i);
			producer.send(transformer.producerTransform(session, producer, message));
		}
		
		session.commit();
		session.close();
		connection.close();
	}

}
