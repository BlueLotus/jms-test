package com.sishu.jmstest.message;

import java.io.InputStream;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.BlobMessage;

import com.sishu.jmstest.broker.util.ConnectionConstantList;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class BlobMessageReceiver {
	
	public static void main(String[] args) throws Exception {
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ConnectionConstantList.WIRELESS_BROKER1);
		Connection connection = connectionFactory.createConnection();
		connection.start();
		
		final Session session = connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);
		Destination destination = session.createQueue("my-queue");
		MessageConsumer consumer = session.createConsumer(destination);

		consumer.setMessageListener(new MessageListener() {
			@Override
			public void onMessage(Message message) {
				if(message instanceof BlobMessage) {
					BlobMessage blobMessage = (BlobMessage) message;
					try {
						InputStream inputStream = blobMessage.getInputStream();
						byte bs[] = new byte[inputStream.available()];
						inputStream.read(bs);
						inputStream.close();
						System.out.println("Content: " + new String(bs));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		});
	}
	
}
