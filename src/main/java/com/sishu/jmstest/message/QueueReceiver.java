package com.sishu.jmstest.message;

import java.util.Enumeration;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MapMessage;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import com.sishu.jmstest.broker.util.ConnectionConstantList;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class QueueReceiver {
	
	public static void main(String[] args) throws Exception {
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ConnectionConstantList.WIRELESS_BROKER1);
		Connection connection = connectionFactory.createConnection();
		connection.start();
		
		final Session session = connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);
		
		Destination destination = session.createQueue("my-queue-2");
		
		MessageConsumer consumer = session.createConsumer(destination);
		
		int i = 0;
		while(i < 3) {
			
			MapMessage mapMessage = (MapMessage) consumer.receive();
			System.out.println("Receive: " + mapMessage.getString("Message: " + i));
			i++;
			session.commit();
		}
		
		session.close();
		connection.close();
	}
	
}
