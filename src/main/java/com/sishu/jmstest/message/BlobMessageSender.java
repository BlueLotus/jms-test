package com.sishu.jmstest.message;

import java.io.File;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ActiveMQSession;
import org.apache.activemq.BlobMessage;

import com.sishu.jmstest.broker.util.ConnectionConstantList;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class BlobMessageSender {
	
	public static void main(String[] args) throws Exception {
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ConnectionConstantList.WIRELESS_BROKER_WITH_FILE_SERVER);
		Connection connection = connectionFactory.createConnection();
		connection.start();
		
		ActiveMQSession session = (ActiveMQSession) connection.createSession(Boolean.TRUE, Session.CLIENT_ACKNOWLEDGE);
		Destination destination = session.createQueue("my-queue");
		MessageProducer producer = session.createProducer(destination);
		BlobMessage blobMessage = session.createBlobMessage(new File("pom.xml"));
		producer.send(blobMessage);
		
		session.commit();
		session.close();
		connection.close();
	}

}
